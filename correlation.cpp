/*************************************************************************
	Copyright (C) 2023 Nicolas L. Guidotti. All rights reserved.

	This file is part of the MOCCA library, which is licensed under 
	the terms contained in the LICENSE file.
 **************************************************************************/

#include "common.h"

int main(int argc, char* argv[])
{
    if (argc != 4)
    {
        fmt::print(stderr, "Invalid Parameters. Usage: ./{} <algorithm> <filename> <gamma>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    std::string algorithm = argv[1];
    std::string filename = argv[2];
    double gamma = atof(argv[3]);

    Vector ref, res;

    std::string name = fmt::format("output/{}-{}-{}-total-comm.h5", algorithm, filename, gamma);
    std::string refname = fmt::format("reference/{}-{}-total-comm.h5", filename, gamma);
    mocca::io::read_hdf5(name, "result", res);
    mocca::io::read_hdf5(refname, "result", ref);

    Vector ref_rank_100;
    Vector rank_100;

    rankdata(ref, ref_rank_100);
    rankdata(res, rank_100);

    Vector rank_10 = mocca::init_vector<double>(0.1 * ref.size(), [&](auto i){
        return rank_100[i];
    });

    Vector rank_1 = mocca::init_vector<double>(0.01 * ref.size(), [&](auto i){
        return rank_100[i];
    });

    Vector ref_rank_10 = mocca::init_vector<double>(0.1 * ref.size(), [&](auto i){
        return ref_rank_100[i];
    });

    Vector ref_rank_1 = mocca::init_vector<double>(0.01 * ref.size(), [&](auto i){
        return ref_rank_100[i];
    });

    double corr_10 = correlation(rank_10, ref_rank_10);
    double corr_1 = correlation(rank_1, ref_rank_1);

    fmt::print("Correlation TOP10%: {}\n", corr_10);
    fmt::print("Correlation TOP1%: {}\n", corr_1);

    return EXIT_SUCCESS;
}