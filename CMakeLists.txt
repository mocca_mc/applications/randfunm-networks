cmake_minimum_required(VERSION 3.17)
project(RandNetwork)

# MOCCA library
set(MOCCA_DIR ${CMAKE_CURRENT_LIST_DIR}/MOCCA/)
set(MOCCA_BLAS amd-aocl)
set(ENABLE_HDF5 ON)
find_package(MOCCA REQUIRED)

# Compiler Configuration
add_library(compiler_config INTERFACE)
target_compile_options(compiler_config INTERFACE -Wall -Wno-enum-compare -march=native -O3)
target_link_libraries(compiler_config INTERFACE MOCCA::MOCCA)
target_compile_features(compiler_config INTERFACE cxx_std_17)

# Generate references
add_executable(generate_refs generate_refs.cpp)
target_link_libraries(generate_refs compiler_config)

add_subdirectory(${MOCCA_DIR}/utils/ ${CMAKE_BINARY_DIR}/utils/)

# Parallel Executables
add_executable(subgraph subgraph.cpp)
target_link_libraries(subgraph PUBLIC compiler_config)

add_executable(total_comm total_comm.cpp)
target_link_libraries(total_comm PUBLIC compiler_config)

add_executable(total_comm_single total_comm_single.cpp)
target_link_libraries(total_comm_single PUBLIC compiler_config)

add_executable(parallel_test parallel_test.cpp)
target_link_libraries(parallel_test PUBLIC compiler_config)

add_executable(katz_centrality katz_centrality.cpp)
target_link_libraries(katz_centrality PUBLIC compiler_config)

add_executable(correlation correlation.cpp)
target_link_libraries(correlation PUBLIC compiler_config)

# Add the sequential version of the AMD AOCL (other libraries should work as long as we modify below)
if (MOCCA_BLAS STREQUAL "amd-aocl")
    add_library(blas_seq INTERFACE)
    target_link_directories(blas_seq INTERFACE $ENV{AOCLROOT}/lib/intel64/)
    target_compile_definitions(blas_seq INTERFACE MOCCA_BLAS=AOCL)
    target_link_options(blas_seq INTERFACE -L${AOCLROOT}/lib/ -lblis -Wl,-rpath -Wl,${AOCLROOT}/lib/)
else ()
    add_library(blas_seq INTERFACE)
    target_compile_definitions(blas_seq INTERFACE MOCCA_BLAS=MKL)
    target_link_directories(blas_seq INTERFACE $ENV{MKL_ROOT}/lib/intel64/)
    target_compile_options(blas_seq INTERFACE -DMKL_ILP64 -I"${MKL_ROOT}/include")
    target_link_options(blas_seq INTERFACE  -L${MKL_ROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_intel_ilp64 -lmkl_intel_thread -lmkl_core)
endif ()

add_executable(speed_test speed_test.cpp)
target_include_directories(speed_test PUBLIC ${MOCCA_INCLUDE})
target_compile_options(speed_test PUBLIC -Wall -Wno-enum-compare -fopenmp -march=native -O3)
target_link_options(speed_test PUBLIC -lm -fopenmp -lpthread -ldl -lhdf5 -lz)
target_link_libraries(speed_test PUBLIC fmt::fmt blas_seq)
target_compile_features(speed_test PUBLIC cxx_std_17)
