import subprocess
import sys

def run_acc_test(centrality, graphs, max_size, max_samples, gamma):
    ds = "{}".format(int(1E8))
    dt = "1E-6"

    program = "./build/{}".format(centrality)
    error_log = open("{}.log".format(centrality), "a")
    out_acc = open("{}-acc.csv".format(centrality), "a")
    out_scale = open("{}-scale.csv".format(centrality), "a")

    samples = [10 ** x for x in range(5, max_samples + 1)]
    samples.extend([2 * 10 ** x for x in range(5, max_samples)])
    samples.extend([5 * 10 ** x for x in range(5, max_samples)])

    print("Type:", centrality)

    for g in graphs:
        for i in range(len(samples)):
            print("Accuracy test: graph = {:>20} | samples = {:>12} | tol = {}".format(g, samples[i], dt))
            p = subprocess.run([program, g, "{}".format(int(samples[i])), dt, gamma], capture_output = True, encoding = "utf-8")
            error_log.write(p.stderr)
            out_acc.write(p.stdout)

        for i in range(0, 11):
            tol = "{}".format(1 / 10 ** i)
            print("Accuracy test: graph = {:>20} | samples = {:>12} | tol = {}".format(g, ds, tol))
            p = subprocess.run([program, g, ds, tol, gamma], capture_output = True, encoding = "utf-8")
            error_log.write(p.stderr)
            out_acc.write(p.stdout)

        out_acc.flush()
        error_log.flush()

    for i in range(12, max_size + 1):
            kronecker = "kronecker-{}-16".format(i)
            print("Accuracy test: graph = {:>20} | samples = {:>12} | tol = {}".format(kronecker, ds, dt))
            p = subprocess.run([program, kronecker, ds, dt, gamma], capture_output = True, encoding = "utf-8")
            error_log.write(p.stderr)
            out_scale.write(p.stdout)

            smallworld = "smallworld-{}-10-0.1".format(i)
            print("Accuracy test: graph = {:>20} | samples = {:>12} | tol = {}".format(smallworld, ds, dt))
            p = subprocess.run([program, smallworld, ds, dt, gamma], capture_output = True, encoding = "utf-8")
            error_log.write(p.stderr)
            out_scale.write(p.stdout)
            out_scale.flush()
            error_log.flush()

    error_log.close()
    out_scale.close()
    out_acc.close()


def run_acc_single(centrality, graphs, gamma):
    ds = "{}".format(int(1E8))
    dt = "1E-6"

    program = "./build/{}_single".format(centrality)
    error_log = open("{}.log".format(centrality), "a")
    out_acc = open("{}-single.csv".format(centrality), "a")

    print("Type:", centrality)

    for g in graphs:
        print("Single entry test: graph = {:>20} | samples = {:>12} | tol = {}".format(g, ds, dt))
        p = subprocess.run([program, g, ds, dt, gamma], capture_output = True, encoding = "utf-8")
        error_log.write(p.stderr)
        out_acc.write(p.stdout)
        out_acc.flush()
        error_log.flush()

    error_log.close()
    out_acc.close()

def speed_test(centrality, algorithm, tests, gamma):
    print("Type:", centrality)

    log = open("{}.log".format(centrality), "a")
    out = open("{}-speed.csv".format(centrality), "a")

    for test in tests:
        print("Speed Test: algorithm = {:>20} | graph = {:>15} | samples = {:>12}".format(algorithm, test[0], test[1]))
        p = subprocess.run(["./build/speed_test", centrality, algorithm, test[0], str(test[1]), str(gamma)],
                           capture_output = True, encoding = "utf-8")
        log.write(p.stderr)
        out.write(p.stdout)
        log.flush()
        out.flush()

    log.close()
    out.close()


def strong_scaling(centrality, tests, gamma):
    print("Type:", centrality)

    log = open("{}.log".format(centrality), "a")
    out = open("{}-strong.csv".format(centrality), "a")
    for test in tests:
        print("Strong scaling: graph = {:>20} | samples = {:>12}".format(test[0], test[1]))
        p = subprocess.run(["./build/parallel_test", centrality, test[0], str(test[1]), str(gamma)],
                           capture_output = True, encoding = "utf-8")
        log.write(p.stderr)
        out.write(p.stdout)
        log.flush()
        out.flush()
    log.close()
    out.close()

def katz_centrality(tests, samples):
    print("Type: Katz Centrality")

    log = open("katz_centrality.log", "a")
    out = open("katz-centrality.csv", "a")
    for test in tests:
        print("graph = {:>20} | samples = {:>12}".format(test, samples))
        p = subprocess.run(["./build/katz_centrality", test, str(samples)], capture_output = True, encoding = "utf-8")
        log.write(p.stderr)
        out.write(p.stdout)
        log.flush()
        out.flush()

    log.close()
    out.close()

if __name__ == "__main__":

    if (len(sys.argv) != 2):
        print("Usage: ./{} <setup|matlab|accuracy|single-entry|strong-scaling|speed-subgraph|speed-total-comm|katz-centrality>")

    if "setup" in sys.argv[1]:
        subprocess.run(["cmake", "-B", "build/", "-DCMAKE_CXX_COMPILER=clang++", "-DCMAKE_C_COMPILER=clang"])
        subprocess.run(["make", "-C", "build/", "-j8"])

        for i in range(12, 26):
            print("Generating kronecker graph (n = 2 ** {})... ".format(i), end = "")
            subprocess.run(["./build/utils/kronecker", str(i), "16"])
            print("Done")

            print("Generating smallworld network (n = 2 ** {})... ".format(i), end = "")
            subprocess.run(["./build/utils/smallworld", str(i), "5", "0.1"])
            print("Done")

        subgraph_graphs = ["smallworld-{}-10-0.1".format(i) for i in range(15, 20)]
        subgraph_graphs.extend(["kronecker-{}-16".format(i) for i in range(16, 20)])

        # The default number of random samples is 1E11 for all graphs
        for g in subgraph_graphs:
            print("Generating references for {}... ".format(g), end = "")
            subprocess.run(["./build/generate_refs", g, "{}".format(int(1E11)), "1E-8", "1E-3"])
            print("Done")

    elif "matlab" in sys.argv[1]:
        subprocess.run(["matlab", "-sd", "matlab/", "-batch", "eval_matlab; exit;"])

    elif "accuracy" in sys.argv[1]:
        run_acc_test("subgraph", graphs = ["stanford"],
                     max_size = 19, max_samples = 10, gamma = "1E-3")
        run_acc_test("total_comm", graphs = ["kronecker-24-16", "smallworld-24-10-0.1", "orkut"], max_size = 25,
                     max_samples = 10, gamma = "1E-5")

    elif "single-entry" in sys.argv[1]:
        run_acc_single("total_comm", graphs = ["kronecker-24-16", "smallworld-24-10-0.1", "stanford", "orkut"], gamma = "1E-5")

    elif "strong-scaling" in sys.argv[1]:
        tests = [("kronecker-24-16", 10000000000),
                 ("smallworld-24-10-0.1", 400000000000),
                 # ("twitter", 10000000000),                         # Uncomment this line if you want to test the large instances
                 # ("uk-2005", 100000000000),                         # Uncomment this line if you want to test the large instances
                 ("orkut", 64000000000)]
        strong_scaling("total-comm", tests, 1E-5)

        tests = [("kronecker-19-16", 4000000000),
                 ("smallworld-19-10-0.1", 100000000000),
                 ("twitch", 5000000000),
                 ("stanford", 20000000000)]
        strong_scaling("subgraph", tests, 1E-3)

    elif "speed-subgraph" in sys.argv[1]:

        tests = [("yeast", 320000),
                 ("power-us", 100000),
                 ("internet", 200000000),
                 ("cond-mat", 200000000)]
        tests.extend([("kronecker-{}-16".format(i), int(20000 * (2 * i - 23) ** 2.3)) for i in range(12, 16)])
        tests.extend([("smallworld-{}-10-0.1".format(i), int(200000 * (i - 11) ** 2)) for i in range(12, 15)])
        speed_test("subgraph", "RandFunm", tests, 1E-3)
        tests.extend([("twitch", 400000000),
                      ("stanford", 100000000)])
        speed_test("subgraph", "RandFunmDiag", tests, 1E-3)

        tests = [("yeast", 100000000000),
                 ("power-us", 10000000000),
                 ("internet", 10000000000),
                 ("cond-mat", 10000000000)]
        speed_test("subgraph", "MC", tests, 1E-3)

    elif "speed-total-comm" in sys.argv[1]:
        tests = [("yeast", 3000),
                 ("power-us", 10000),
                 ("internet", 37000),
                 ("cond-mat", 60000),
                 ("twitch", 1000000),
                 ("stanford", 5000000),
                 # ("uk-2005", 250000000), # Uncomment this line if you want to test the large instances
                 # ("twitter", 1000000000), # Uncomment this line if you want to test the large instances
                 ("orkut", 10000000)]
        tests.extend([("kronecker-14-16", 36000), ("kronecker-15-16", 80000), ("kronecker-16-16", 160000)])
        tests.extend([("kronecker-{}-16".format(i), int(400000 * (i - 16) ** 2)) for i in range(17, 26)])
        tests.extend([("smallworld-{}-10-0.1".format(i), int(800000 * (i - 13) ** 2)) for i in range(14, 26)])
        speed_test("total-comm", "RandFunmAction", tests, 1E-5)

    elif "katz-centrality" in sys.argv[1]:
        katz_centrality(["kronecker-24-16", "orkut", "uk-2005"], 1E9)
