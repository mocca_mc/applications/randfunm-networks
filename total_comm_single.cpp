/*************************************************************************
    Copyright (C) 2022 Nicolas L. Guidotti. All rights reserved.

    This file is part of the MOCCA library, which is licensed under
    the terms contained in the LICENSE file.
 **************************************************************************/

#define MOCCA_NO_DEBUG

#include "common.h"
#include "mocca/monte_carlo/rand_funm.h"
#include "mocca/monte_carlo/classical_mc.h"


void print_results(double res, const double ref, std::string filename, std::string centrality, std::string algorithm,
                   index_t rows, index_t nz, double alpha, double gamma, index_t samples, double time, int nt,
                   double tol)
{
    double abs_error = abs(res - ref);
    double rel_error = abs_error / abs(ref);
    double l2_error = abs_error;

    fmt::print("{},", centrality);
    fmt::print("{},", algorithm);
    fmt::print("{},", nt);
    fmt::print("{},", filename);
    fmt::print("{},", rows);
    fmt::print("{},", nz);
    fmt::print("{:.3g},", alpha);
    fmt::print("{:.3g},", gamma);
    fmt::print("{},", samples);
    fmt::print("{:.3g},", tol);
    fmt::print("{:.6g},", abs_error);
    fmt::print("{:.6g},", rel_error);
    fmt::print("{:.6g},", l2_error);
    fmt::print("-1,");
    fmt::print("-1,");
    fmt::print("{:.3g}\n", time);
}

int main(int argc, char* argv[])
{
    if (argc != 5)
    {
        fmt::print(stderr, "Invalid Parameters. Usage: ./{} <filename> <num_samples> <tol> <gamma>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    std::string filename = argv[1];
    index_t samples = atol(argv[2]);
    double tol = atof(argv[3]);
    double gamma = atof(argv[4]);

    mocca::RandFunm<CSRMatrix, mocca::kUniformProbability> rand_funm;
    mocca::ClassicalMC<CSRMatrix, mocca::kUniformProbability> classical_mc;
    mocca::random::SplitMix64 seed_gen;

    double res = 0;
    auto [A, ref] = import_data("total-comm", filename, gamma);
    Vector vec(A.cols(), 1);
    Vector A_row_sum = norm_l1(A, mocca::rowwise);
    gamma = gamma == 0 ? 1.0 / max(A_row_sum) : gamma;

    index_t max_idx = 0;
    double max_val = -INFINITY;
    for (index_t i = 0; i < A_row_sum.size(); ++i)
    {
        if (A_row_sum[i] > max_val)
        {
            max_idx = i;
            max_val = A_row_sum[i];
        }
    }

    double set_time = -omp_get_wtime();
    rand_funm.set_matrix(A);
    set_time += omp_get_wtime();

    double set_time_classical = -omp_get_wtime();
    classical_mc.set_matrix(A);
    set_time_classical += omp_get_wtime();

    for (int trial = 0; trial < NUM_TRIALS_ACC; ++trial)
    {
        uint64_t seed = seed_gen();
        uint64_t stream = seed_gen();

        rand_funm.set_param({.num_samples = samples,
                             .max_num_terms = MAX_TERMS,
                             .weight_cutoff = tol,
                             .seed = seed,
                             .stream = stream});

        classical_mc.set_param({.num_samples = samples,
                                .max_num_terms = MAX_TERMS,
                                .weight_cutoff = tol,
                                .seed = seed,
                                .stream = stream});

        double t0 = omp_get_wtime();
        res = rand_funm.funm_single(mocca::expm, max_idx, vec, gamma);
        double t1 = omp_get_wtime();

        print_results(res, ref[max_idx], filename, "total-comm-single", "RandFunmAction", A.rows(), A.size(), 1.0, gamma, samples,
                      t1 - t0 + set_time, omp_get_max_threads(), tol);

        t0 = omp_get_wtime();
        res = classical_mc.funm_single(mocca::expm, max_idx, vec, gamma);
        t1 = omp_get_wtime();

        print_results(res, ref[max_idx], filename, "total-comm-single", "ClassicalMC", A.rows(), A.size(), 1.0, gamma, samples,
                      t1 - t0 + set_time_classical, omp_get_max_threads(), tol);
    }

    return EXIT_SUCCESS;
}
