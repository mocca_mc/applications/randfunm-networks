/*************************************************************************
    Copyright (C) 2022 Nicolas L. Guidotti. All rights reserved.

    This file is part of the MOCCA library, which is licensed under
    the terms contained in the LICENSE file.
 **************************************************************************/

#define MOCCA_NO_DEBUG
#define TOL 1E-8

#include "common.h"
#include "mocca/monte_carlo/rand_funm.h"


int main(int argc, char* argv[])
{
    if (argc != 5)
    {
        fmt::print(stderr, "Invalid Parameters. Usage: ./{} <centrality> <filename> <num_samples> <gamma>\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    
    std::string type = argv[1];
    std::string filename = argv[2];
    index_t samples = atol(argv[3]);
    double gamma = atof(argv[4]);

    std::string alg = type == "subgraph" ? "RandFunmDiag" : "RandFunmAction";
    
    mocca::RandFunm<CSRMatrix, mocca::kUniformProbability> monte_carlo;
    mocca::random::SplitMix64 seed_gen;
    Vector res;
    auto [A, ref] = import_data(type, filename, gamma);
    Vector vec(A.cols(), 1);
    gamma = gamma == 0 ? 1.0 / max(norm_l1(A, mocca::rowwise)) : gamma;

    Vector ref_rank(ref.size());
    rankdata(ref, ref_rank);

    double set_time = -omp_get_wtime();
    monte_carlo.set_matrix(A);
    set_time += omp_get_wtime();
    
    for (int nt : {64, 56, 48, 40, 32, 24, 16, 8, 1})
    {
        omp_set_num_threads(nt);
        
        index_t n = NUM_TRIALS_SPEED;

        for (int trial = 0; trial <  n; ++trial)
        {
            index_t ns = nt * samples;
            monte_carlo.set_param({.num_samples = ns,
                                   .max_num_terms = MAX_TERMS,
                                   .weight_cutoff = TOL,
                                   .seed = seed_gen(),
                                   .stream = seed_gen()});
            
            double t0 = omp_get_wtime();
            if (type == "total-comm") res = monte_carlo.funm(mocca::expm, vec, gamma);
            else res = monte_carlo.funm_diag(mocca::expm, gamma);
            double t1 = omp_get_wtime();
            
            print_results(res, ref, ref_rank, filename, type, alg, A.rows(), A.size(), 1.0, gamma, ns,
                          t1 - t0 + set_time, nt, TOL);
            fflush(stdout);
            
        }
    }
    
    return EXIT_SUCCESS;
}
