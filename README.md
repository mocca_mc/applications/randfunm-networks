# RandFunm - Complex Networks

This repository contains the test suite used for evaluating the `RandFunm` algorithm from the [MOCCA](https://gitlab.com/moccalib/MOCCA) library. In particular, we calculate the subgraph centrality [1, 2] and total communicability [3] of several synthetic and real complex networks. These metrics are based on the matrix exponential $e^{\gamma \mathbf{A}}$, where $\mathbf{A}$ is the adjacency matrix and $\gamma \in [0, 1]$. In one of the tests, we also estimate the Katz's centrality [4] of complex networks, which is based on the matrix resolvent $(\mathbf{I} - \gamma \mathbf{A})^{-1}\vec{1}$.

The paper describing the `RandFunm` algorithm was submitted to the Journal of Scientific Computing and its preprint is available in [ArXiv](https://arxiv.org/abs/2308.01037).

## Datasets

|          Netwoks         | Included? | File Format |                                                         Source                                                         |
|:------------------------:|:---------:|:-----------:|:----------------------------------------------------------------------------------------------------------------------:|
|    `kronecker-<n>-16`\*  |     X     |     hdf5    |                                           [Graph500](https://graph500.org/)\*\*                                        |
| `smallworld-<n>-10-0.1`\*|     X     |     hdf5    |                                                Watts-Strogatz networks\*\*                                             |
|          `yeast`         |     X     |     hdf5    |                   [Pajek datasets](http://vlado.fmf.uni-lj.si/pub/networks/data/bio/Yeast/Yeast.htm)                   |
|        `power-us`        |     X     |     hdf5    |                            [Newman repository](http://www-personal.umich.edu/~mejn/netdata/)                           |
|        `internet`        |     X     |     hdf5    |                            [Newman repository](http://www-personal.umich.edu/~mejn/netdata/)                           |
|        `cond-mat`        |     X     |     hdf5    |                            [Newman repository](http://www-personal.umich.edu/~mejn/netdata/)                           |
|         `twitch`         |     X     |     hdf5    |                                [SNAP](http://snap.stanford.edu/data/twitch_gamers.html)                                |
|        `stanford`        |     X     |     hdf5    | [SNAP](http://snap.stanford.edu/data/web-Stanford.html), [SuiteSparse Matrix Collection](http://sparse.tamu.edu/SNAP) |
|        `orkut`\*\*\*     |           |     mtx     | [SNAP](http://snap.stanford.edu/data/com-Orkut.html), [SuiteSparse Matrix Collection](http://sparse.tamu.edu/SNAP) |
|        `uk-2005`\*\*\*   |           |     mtx     |   [LAW](https://law.di.unimi.it/webdata/uk-2005), [SuiteSparse Matrix Collection](http://sparse.tamu.edu/LAW/uk-2005)  |
|        `twitter`\*\*\*   |           |     mtx     | [SNAP](http://snap.stanford.edu/data/twitter7.html), [SuiteSparse Matrix Collection](http://sparse.tamu.edu/SNAP) |


\* Here, the number of nodes is equal to $\sim 2^n$.

\*\* Both `kronecker-<n>-16` and `smallworld-<n>-10-0.1` will be generated automatically during the setup.

\*\*\* `orkut`, `uk-2005` and `twitter` are not included in this repository due to their size. So, you must download them from the [SuiteSparse Matrix Collection](http://sparse.tamu.edu) and then convert them from mtx to hdf5 format using the `mtx2hdf5` program located in the folder `build/utils/` (after the [setup](Installation)). The arguments are `./build/utils/mtx2hdf5 <path to file without extensions> <symmetrize?>`, set `<symmetrize>` to `1` for `uk-2005` and `twitter` (see [5]), `0` for `orkut`.

**Important:** The `uk-2005` and `twitter` network consume up to **100 GB or more of memory** during testing and for generating the `hdf5` files, and thus, are disabled by default. You can enable these networks by uncommenting them in the source files (python and matlab scripts). 


## Related Software

The following packages were used in this test suite:

- [expm](https://www.mathworks.com/help/matlab/ref/expm.html), see [6]
- [expmv](https://github.com/higham/expmv), see [7]
- [funm_kryl](http://guettel.com/funm_kryl/), see [8]
- [sFOM](https://github.com/MarcelSchweitzer/sketched_fAb), see [9]
- [rand_kryl](https://github.com/Alice94/RandMatrixFunctions), see [10]

For the "classical" Monte Carlo method for matrix functions, we adapted the code from [11]. It is implemented as `ClassicalMC` in the `MOCCA` library.

## Requirements

- CMake v3.20+
- C++ compiler with support for OpenMP v4.5 and C++17 (recommended: Clang v14.0+ or [AMD AOCC v4.0+](https://www.amd.com/en/developer/aocc.html))
- [AMD AOCL](https://www.amd.com/en/developer/aocl.html) or [Intel MKL](https://www.intel.com/content/www/us/en/developer/tools/oneapi/onemkl.html)
- [HDF5](https://www.hdfgroup.org/solutions/hdf5/) (must include the headers)
- [{fmt} library](https://github.com/fmtlib/fmt)

## Installation

Before using this repository, set the environment variable `AOCL_ROOT` to where `AMD AOCL` is located. Alternatively, you can change the variable `MOCCA_BLAS` in the `CMakeLists.txt` to `intel-mkl` to use the [Intel MKL](https://www.intel.com/content/www/us/en/developer/tools/oneapi/onemkl.html) package. Note that the Intel MKL must contain the `MKLConfig.cmake` script (that is usually included in the develoment package of MKL or from the manual installation).

Then, clone this repository with the [MOCCA](https://gitlab.com/moccalib/MOCCA) library as a submodule (see [git submodule's documentation](https://git-scm.com/book/en/v2/Git-Tools-Submodules) for more details):

```bash
git clone --recurse-submodules https://gitlab.com/moccalib/applications/randfunm-networks.git
```

Next, execute

```bash
python3 ./run_tests.py setup
```

to create all synthetic graphs as well as their reference ranking lists. This may take a few hours depending on the hardware of your machine.

## Usage


### MATLAB

It is important to run the MATLAB test **before** any other tests as it will generate the reference ranking list for the other tests.

```bash
python3 ./run_tests.py matlab
```

**Note:** For `uk-2005` and `twitter` networks, the computation of the correlation is too slow in MATLAB, and thus, need to be calculated manually using the `correlation` program and the output files from the simulation after this test is completed.

### Accuracy tests

This will evaluate the error of `RandFunmDiag` and `RandFunmAction` in function of the number of random walks $N_s$, ranging from $N_s = 10^5$ to $N_s = 10^{10}$, and the weight cutoff $W_c$, ranging from $W_c = 1$ to $W_c = 10^{-10}$. This will also measure the error in function of the number of nodes in the graph, ranging from $n \approx 2^{12}$ to $n \approx 2^{24}$. You can change the maximum $N_s$ and $n$ by changing `max_samples` and `max_size` in the python script, respectively.

```bash
python3 ./run_tests.py accuracy

```

### Performance comparison

This test will measure the execution time, accuracy and correlation of the Monte Carlo methods for estimating the subgraph centrality and total communicability.

```bash
python3 ./run_tests.py speed-subgraph

python3 ./run_tests.py speed-total-comm
```

### Strong scaling tests

This test will run with `[64, 56, 48, 40, 32, 24, 16, 8, 1]` cores. To set other cores configurations, change the `parallel_test.cpp` code with the desire values. Note that the tests were dimensioned to have a reasonable time with `64` cores.

```bash
python3 ./run_tests.py strong-scaling

```

### Single entry

This test will estimate the total communicability of the node with the highest degree in the graph.

```bash
python3 ./run_tests.py single-entry

```

### Katz's Centrality

This test will estimate the Katz's centrality of a few networks and compare it against a simple Conjugate Gradient method.

```bash
python3 ./run_tests.py katz-centrality

```

## Output

The test results will be stored in a `.csv` file. The columns names are the following:

| Centrality | Algorithm | Threads | Network | Nodes | Edges | $\alpha$ | $\gamma$ | $N_s$ | $W_c$ | Max. abs. Error | Max. rel. error | $\ell 2$ error | Correlation Top-10% | Correlation Top-1% | Execution time |
|------------|-----------|---------|---------|-------|-------|----------|----------|-------|-------|-----------------|-----------------|----------------|----------------|----------------|----------------|

Here, a value of `-1` or `NaN` indicate that the data is not applicable in this test.

## Authors

Nicolas L. Guidotti, Juan A. Acebrón and José Monteiro

## License

[Lesser GPL v3](https://www.gnu.org/licenses/lgpl-3.0.en.html)

## References

[1] E. Estrada and J. A. Rodríguez-Velázquez, “Subgraph centrality in complex networks,” Phys. Rev. E, vol. 71, no. 5, p. 056103, May 2005, doi: 10.1103/PhysRevE.71.056103.

[2] E. Estrada and D. J. Higham, “Network Properties Revealed through Matrix Functions,” SIAM Rev., vol. 52, no. 4, pp. 696–714, Jan. 2010, doi: 10.1137/090761070.

[3] M. Benzi and C. Klymko, “Total communicability as a centrality measure,” Journal of Complex Networks, vol. 1, no. 2, pp. 124–149, Dec. 2013, doi: 10.1093/comnet/cnt007.

[4] L. Katz, “A new status index derived from sociometric analysis,” Psychometrika, vol. 18, no. 1, pp. 39–43, Mar. 1953, doi: 10.1007/BF02289026.

[5] M. Benzi, E. Estrada, and C. Klymko, “Ranking hubs and authorities using matrix functions,” Linear Algebra and its Applications, vol. 438, no. 5, pp. 2447–2474, Mar. 2013, doi: 10.1016/j.laa.2012.10.022.

[6] A. H. Al-Mohy and N. J. Higham, “A New Scaling and Squaring Algorithm for the Matrix Exponential,” SIAM J. Matrix Anal. Appl., vol. 31, no. 3, pp. 970–989, Jan. 2010, doi: 10.1137/09074721X.

[7] A. H. Al-Mohy and N. J. Higham, “Computing the Action of the Matrix Exponential, with an Application to Exponential Integrators,” SIAM Journal on Scientific Computing, vol. 33, no. 2, Art. no. 2, Mar. 2011, doi: 10.1137/100788860.

[8] M. Afanasjew, M. Eiermann, O. G. Ernst, and S. Güttel, “Implementation of a restarted Krylov subspace method for the evaluation of matrix functions,” Linear Algebra and its Applications, vol. 429, no. 10, pp. 2293–2314, Nov. 2008, doi: 10.1016/j.laa.2008.06.029.

[9] S. Güttel and M. Schweitzer, “Randomized sketching for Krylov approximations of large-scale matrix functions.” arXiv, Mar. 17, 2023. Accessed: Oct. 30, 2023. [Online]. Available: http://arxiv.org/abs/2208.11447

[10] A. Cortinovis, D. Kressner, and Y. Nakatsukasa, “Speeding up Krylov subspace methods for computing f(A)b via randomization.” arXiv, Jun. 05, 2023. Accessed: Oct. 27, 2023. [Online]. Available: http://arxiv.org/abs/2212.12758

[11] M. Benzi, T. M. Evans, S. P. Hamilton, M. Lupo Pasini, and S. R. Slattery, “Analysis of Monte Carlo accelerated iterative methods for sparse linear systems,” Numerical Linear Algebra with Applications, vol. 24, no. 3, 2017, doi: 10.1002/nla.2088.


