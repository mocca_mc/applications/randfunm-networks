function export_hdf5(filename, x)
    fileID = H5F.create(filename, "H5F_ACC_TRUNC", "H5P_DEFAULT" , "H5P_DEFAULT" );
    h5create(filename, "/result", size(x));
    h5write(filename, "/result", x);
    h5writeatt(filename, "/result", "mat_type", 1);
    h5writeatt(filename, "/result", "num_rows", int64(size(x, 1)));
    h5writeatt(filename, "/result", "num_cols", int64(size(x, 2)));
    H5F.close(fileID);
end
