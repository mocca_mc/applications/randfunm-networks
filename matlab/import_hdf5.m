function A = import_hdf5(filename)

row_offset = int64(h5read(filename, "/A/row_offset")) + 1;
columns = int64(h5read(filename, "/A/nz_columns")) + 1;
rows = int64(zeros(size(columns)));

n = size(row_offset, 1) - 1;

for i = 1:n;
    rows(row_offset(i):row_offset(i + 1) - 1) = int64(i);
end

A = sparse(rows, columns, 1, n, n);

end
