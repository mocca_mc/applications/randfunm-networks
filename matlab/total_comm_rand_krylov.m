function r = total_comm_rand_krylov(filename, m, gamma, save_output)

addpath rand_kryl/;

A = import_hdf5("../input/" + filename + ".h5");
v = ones(size(A, 2), 1);
norm_v = norm(v);
v = v / norm_v;
A = A * gamma;

tic;
Afun = @(x) A*x;
f = @(X, y) expm(full(X))*y;
[V, H, whitened] = Basis_NakatsukasaTroppWhite(size(A, 2), Afun, v, m);
x = lsqr(V(:,1:end-1), H(end,end)*V(:,end), 1e-6, 1000);
M = H(1:m, :); M(:,end) = M(:,end) + x;
y = V(:,1:m) * f(M, [1/norm(V(:,1)); zeros(m-1, 1)]) * norm_v;
elapsed = toc;

ref = h5read("../reference/" + filename + "-" + gamma + "-total-comm.h5", "/result");
abs_err = norm(y - ref, "inf");
rel_err = norm(y - ref, "inf") / norm(ref, "inf");
l2_err = norm(y - ref, 2);

if (save_output)
    corr_1 = NaN
    corr_10 = NaN
    file = "../output/rand-kryl-" + filename + "-" + gamma + "-total-comm.h5";
    export_hdf5(file, y);
else
    [corr_10, corr_1] = correlation(y, ref);
end


r = sprintf("total-comm,rand_kryl,1,%s,%ld,%ld,1,%.2g,-1,-1,%g,%g,%g,%g,%g,%.6f\n", filename, size(A, 2), nnz(A), gamma, abs_err, rel_err, l2_err, corr_10, corr_1, elapsed);

rmpath rand_kryl/;
