graphs_sub = ["yeast", "power-us", "internet", "kronecker-12-16", "kronecker-13-16", "kronecker-14-16", "smallworld-12-10-0.1", "smallworld-13-10-0.1", "smallworld-14-10-0.1"];
graphs_total = ["power-us", "cond-mat", "internet", "kronecker-12-16", "kronecker-13-16", "kronecker-14-16", "kronecker-15-16", "kronecker-16-16", "kronecker-17-16", "kronecker-18-16", "kronecker-19-16", "kronecker-20-16", "kronecker-21-16", "kronecker-22-16", "kronecker-23-16", "kronecker-24-16", "smallworld-12-10-0.1", "smallworld-13-10-0.1", "smallworld-14-10-0.1", "smallworld-15-10-0.1", "smallworld-16-10-0.1", "smallworld-17-10-0.1", "smallworld-18-10-0.1", "smallworld-19-10-0.1", "smallworld-20-10-0.1", "smallworld-21-10-0.1", "smallworld-22-10-0.1", "smallworld-23-10-0.1", "smallworld-24-10-0.1", "stanford", "twitch", "yeast"];

trials = 3;
pool = parpool(16);

gamma = 1E-3;
n = trials * size(graphs_sub, 2);
f(1:n) = parallel.FevalFuture;

for i = 1:size(graphs_sub, 2)
    for j = 1:trials
        f(trials * (i - 1) + j) = parfeval(pool, @subgraph_expm, 1, graphs_sub(i), gamma, j == trials);
    end
end
wait(f);
results = fetchOutputs(f);
fprintf(1, "%s", results);

gamma = 1E-5;
n = trials * size(graphs_total, 2);
f(1:n) = parallel.FevalFuture;

for i = 1:size(graphs_total, 2)
    for j = 1:trials
        f(trials * (i - 1) + j) = parfeval(pool, @total_comm_expmv, 1, graphs_total(i), gamma, trials == j);
    end
end
wait(f);
results = fetchOutputs(f);
fprintf(1, "%s", results);

for i = 1:size(graphs_total, 2)
    for j = 1:trials
        f(trials * (i - 1) + j) = parfeval(pool, @total_comm_krylov, 1, graphs_total(i), 1E-8, 40, gamma, 0);
    end
end
wait(f);
results = fetchOutputs(f);
fprintf(1, "%s", results);

for i = 1:size(graphs_total, 2)
    for j = 1:trials
        f(trials * (i - 1) + j) = parfeval(pool, @total_comm_rand_krylov, 1, graphs_total(i), 4, gamma, 0);
    end
end
wait(f);
results = fetchOutputs(f);
fprintf(1, "%s", results);


for i = 1:size(graphs_total, 2)
    for j = 1:trials
        f(trials * (i - 1) + j) = parfeval(pool, @total_comm_sfom, 1, graphs_total(i), 4, gamma, 0);
    end
end
wait(f);
results = fetchOutputs(f);
fprintf(1, "%s", results);

clear f;

maxNumCompThreads(1);

% graphs_large = ["kronecker-25-16", "orkut", "smallworld-25-10-0.1", "uk-2005", "twitter"]
graphs_large = ["kronecker-25-16", "orkut", "smallworld-25-10-0.1"]


for i = 1:2
    r = total_comm_expmv(graphs_large(i), gamma, 1);
    fprintf(1, "%s", r);
end

for i = 1:2
    r = total_comm_krylov(graphs_large(i), 1E-8, 40, gamma, 1);
    fprintf(1, "%s", r);
end

for i = 1:2
    r = total_comm_rand_krylov(graphs_large(i), 4, gamma, 1);
    fprintf(1, "%s", r);
end

for i = 1:2
    r = total_comm_sfom(graphs_large(i), 4, gamma, 1);
    fprintf(1, "%s", r);
end
