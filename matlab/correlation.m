function [corr_10, corr_1] = correlation(x, ref_x)

[~, ix] = sort(x);
[~, iref] = sort(ref_x);

top10 = int64(0.1 * size(x, 1));
top1 = int64(0.01 * size(x, 1));

ix_10 = ix(1:top10);
iref_10 = iref(1:top10);
ix_1 = ix(1:top1);
iref_1 = iref(1:top1);

corr_10 = calculate_corr(ix_10, iref_10);
corr_1 = calculate_corr(ix_1, iref_1);

end

function s = calculate_corr(x, y)

x_mean = mean(x);
y_mean = mean(y);
x_norml2 = norm(x - x_mean);
y_norml2 = norm(y - y_mean);
cov = sum((x - x_mean) .* (y - y_mean));

s = cov / x_norml2 / y_norml2;

end
