function r = total_comm_sfom(filename, m, gamma, save_output)

addpath sFOM/;

A = import_hdf5("../input/" + filename + ".h5");
v = ones(size(A, 2), 1);
norm_v = norm(v);
v = v / norm_v;
A = A * gamma;

tic;
f = @(X, y) expm(full(X))*y;
n = size(A, 2);
k = 2; % how many blocks to orthogonalise against
hS = setup_sketching_handle(n, min(n, 2*m));
[SV,SAV, Vtrunc] = bta_mod(A,v,m, k, hS); % number of mat-vec products = m
[SV, SAV, Rw] = whiten_basis(SV, SAV);
SVm = SV(:,1:m);
M = SVm'*SVm;

coeffs = M \ ( f((SVm'*SAV(:,1:m)) / M , (SVm'*hS(v))));
y = Vtrunc(:,1:m)*(Rw(1:m,1:m)\coeffs) * norm_v;

elapsed = toc;

ref = h5read("../reference/" + filename + "-" + gamma + "-total-comm.h5", "/result");
abs_err = norm(y - ref, "inf");
rel_err = norm(y - ref, "inf") / norm(ref, "inf");
l2_err = norm(y - ref, 2);

if (save_output)
    corr_1 = NaN
    corr_10 = NaN
    file = "../output/sfom-" + filename + "-" + gamma + "-total-comm.h5";
    export_hdf5(file, y);
else
    [corr_10, corr_1] = correlation(y, ref);
end

r = sprintf("total-comm,sfom,1,%s,%ld,%ld,1,%.2g,-1,-1,%g,%g,%g,%g,%g,%.6f\n", filename, size(A, 2), nnz(A), gamma, abs_err, rel_err, l2_err, corr_10, corr_1, elapsed);

rmpath sFOM/;
