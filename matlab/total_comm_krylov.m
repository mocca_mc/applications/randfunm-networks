function r = total_comm_krylov(filename, eps, max_restarts, gamma, save_output)

addpath funm_kryl/;

A = import_hdf5("../input/" + filename + ".h5");
v = ones(size(A, 2), 1);
A = A * gamma;

param = param_init(struct("function", @expm, "max_restarts", max_restarts, "stopping_accuracy", eps, "waitbar", false));
tic;
[x, info] = funm_kryl(A, v, param);
elapsed = toc;

ref = h5read("../reference/" + filename + "-" + gamma + "-total-comm.h5", "/result");
abs_err = norm(x - ref, "inf");
rel_err = norm(x - ref, "inf") / norm(ref, "inf");
l2_err = norm(x - ref, 2);

if (save_output)
    corr_1 = NaN
    corr_10 = NaN
    file = "../output/funm-kryl-" + filename + "-" + gamma + "-total-comm.h5";
    export_hdf5(file, x);
else
    [corr_10, corr_1] = correlation(x, ref);
end

r = sprintf("total-comm,funm_kryl,1,%s,%ld,%ld,1,%.2g,-1,-1,%g,%g,%g,%g,%g,%.6f\n", filename, size(A, 2), nnz(A), gamma, abs_err, rel_err, l2_err, corr_10, corr_1, elapsed);

rmpath funm_kryl/;
