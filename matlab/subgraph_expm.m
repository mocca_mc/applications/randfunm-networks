function r = subgraph_expm(filename, gamma, save)

A = import_hdf5("../input/" + filename + ".h5");
v = ones(size(A, 2), 1);

d_max = max(sum(abs(A), 2));
A = A * gamma;

tic;
x = diag(expm(A));
elapsed_subgraph = toc;

if save > 0
    ref_file = "../reference/" + filename + "-" + gamma + "-subgraph.h5";
    export_hdf5(ref_file, x);
end

r = sprintf("subgraph,expm,1,%s,%ld,%ld,1,%.2g,-1,-1,1E-16,1E-16,1E-16,-1,%g\n", filename, size(A, 2), nnz(A), gamma, elapsed_subgraph);
