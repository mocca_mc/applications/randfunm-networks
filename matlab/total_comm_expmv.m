function r = total_comm_expmv(filename, gamma, save)

addpath expmv/;

A = import_hdf5("../input/" + filename + ".h5");
v = ones(size(A, 2), 1);

tic;
x = expmv(gamma, A, v);
elapsed = toc;

if save > 0
    ref_file = "../reference/" + filename + "-" + gamma + "-total-comm.h5";
    export_hdf5(ref_file, x);
end

r = sprintf("total-comm,expmv,1,%s,%ld,%ld,1,%.2g,-1,-1,%g,%g,%g,1,1,%.6f\n", filename, size(A, 2), nnz(A), gamma, 1E-16, 1E-16, 1E-16, elapsed);

rmpath expmv/;
