/*************************************************************************
    Copyright (C) 2022 Nicolas L. Guidotti. All rights reserved.

    This file is part of the MOCCA library, which is licensed under
    the terms contained in the LICENSE file.
 **************************************************************************/

#include "common.h"
#include "mocca/monte_carlo/rand_funm.h"


int main(int argc, char* argv[])
{
    if (argc != 5)
    {
        fmt::print(stderr, "Invalid Parameters. Usage: ./{} <filename> <num_samples> <tol> <gamma>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    std::string filename = argv[1];
    index_t samples = atol(argv[2]);
    double tol = atof(argv[3]);
    double gamma = atof(argv[4]);
    std::string ref_name = fmt::format("reference/{}-{}-subgraph.h5", filename, gamma);

    mocca::RandFunm<CSRMatrix> monte_carlo({.num_samples = samples, .max_num_terms = 1024, .weight_cutoff = tol});
    CSRMatrix A;
    mocca::io::read_hdf5("input/" + filename + ".h5", "A", A);
    Vector res(A.rows());
    Vector vec(A.cols(), 1);

    gamma = gamma == 0 ? 1.0 / max(norm_l1(A, mocca::rowwise)) : gamma;
    monte_carlo.set_matrix(A);

    fmt::print(stderr, "file = {} | gamma = {:.2g} | samples = {} | tol = {}\n", filename, gamma, samples, tol);

    double t0 = omp_get_wtime();
    res = monte_carlo.funm_diag(mocca::expm, gamma);
    double t1 = omp_get_wtime();

    mocca::io::hdf5::File file(ref_name, {.creation_flags = mocca::io::hdf5::truncate});
    mocca::io::write_hdf5(file, "result", res);
    fmt::print(stderr, "subgraph time = {:.3g}\n", t1 - t0);

    return EXIT_SUCCESS;
}
