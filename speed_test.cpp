/*************************************************************************
    Copyright (C) 2023 Nicolas L. Guidotti. All rights reserved.

    This file is part of the Project library, which is licensed under
    the terms contained in the LICENSE file.
 **************************************************************************/

#define MOCCA_NO_DEBUG
#define MOCCA_NO_MULTITHREADING
#define WEIGHT_CUTOFF 1E-6

#include "common.h"
#include "mocca/monte_carlo/classical_mc.h"
#include "mocca/monte_carlo/rand_funm.h"


void total_comm_rand(std::string graph, CSRMatrix& A, const Vector& ref, double gamma, index_t samples)
{
    mocca::random::SplitMix64 seed_gen;
    mocca::RandFunm<CSRMatrix, mocca::kUniformProbability> monte_carlo;

    Vector vec(A.cols(), 1);
    Vector res(A.rows());

    Vector ref_rank(ref.size());
    rankdata(ref, ref_rank);

    double set_time = -omp_get_wtime();
    monte_carlo.set_matrix(A);
    set_time += omp_get_wtime();

    for (int trial = 0; trial < NUM_TRIALS_SPEED; ++trial)
    {
        monte_carlo.set_param({.num_samples = samples,
                                      .max_num_terms = MAX_TERMS,
                                      .weight_cutoff = WEIGHT_CUTOFF,
                                      .seed = seed_gen(),
                                      .stream = seed_gen()});

        double t0 = omp_get_wtime();
        res = monte_carlo.funm(mocca::expm, vec, gamma);
        double t1 = omp_get_wtime();

        print_results(res, ref, ref_rank, graph, "total-comm", "RandFunmAction", A.rows(), A.size(), 1.0, gamma, samples,
                      t1 - t0 + set_time, 1, WEIGHT_CUTOFF);

    }
}

void subgraph_diag(std::string graph, CSRMatrix& A, const Vector& ref, double gamma, index_t samples)
{
    mocca::random::SplitMix64 seed_gen;
    mocca::RandFunm<CSRMatrix, mocca::kUniformProbability> monte_carlo;

    Vector res(A.rows());

    Vector ref_rank(ref.size());
    rankdata(ref, ref_rank);

    double set_time = -omp_get_wtime();
    monte_carlo.set_matrix(A);
    set_time += omp_get_wtime();

    for (int trial = 0; trial < NUM_TRIALS_SPEED; ++trial)
    {
        monte_carlo.set_param({.num_samples = samples,
                                      .max_num_terms = MAX_TERMS,
                                      .weight_cutoff = WEIGHT_CUTOFF,
                                      .seed = seed_gen(),
                                      .stream = seed_gen()});

        double t0 = omp_get_wtime();
        res = monte_carlo.funm_diag(mocca::expm, gamma);
        double t1 = omp_get_wtime();

        print_results(res, ref, ref_rank, graph, "subgraph", "RandFunmDiag", A.rows(), A.size(), 1.0, gamma, samples,
                      t1 - t0 + set_time, 1, WEIGHT_CUTOFF);

    }
}

void subgraph_MC(std::string graph, CSRMatrix& A, const Vector& ref, double gamma, index_t samples)
{
    mocca::random::SplitMix64 seed_gen;
    mocca::ClassicalMC<CSRMatrix, mocca::kUniformProbability> monte_carlo;

    Matrix result(A.rows(), A.cols());
    Vector res_diag(A.rows());

    Vector ref_rank(ref.size());
    rankdata(ref, ref_rank);

    double set_time = -omp_get_wtime();
    monte_carlo.set_matrix(A);
    set_time += omp_get_wtime();

    for (int trial = 0; trial < NUM_TRIALS_SPEED; ++trial)
    {
        monte_carlo.set_param({.num_samples = samples,
                                      .max_num_terms = MAX_TERMS,
                                      .weight_cutoff = WEIGHT_CUTOFF,
                                      .seed = seed_gen(),
                                      .stream = seed_gen()});

        double t0 = omp_get_wtime();
        result = monte_carlo.funm(mocca::expm, gamma);
        res_diag = diag(result);
        double t1 = omp_get_wtime();

        print_results(res_diag, ref, ref_rank, graph, "subgraph", "MC", A.rows(), A.size(), 1.0, gamma, samples,
                      t1 - t0 + set_time, 1, WEIGHT_CUTOFF);

    }
}

void subgraph_full(std::string graph, CSRMatrix& A, const Vector& ref, double gamma, index_t samples)
{
    mocca::random::SplitMix64 seed_gen;
    mocca::RandFunm<CSRMatrix, mocca::kUniformProbability> monte_carlo;

    Matrix result(A.rows(), A.cols());
    Vector res_diag(A.rows());

    Vector ref_rank(ref.size());
    rankdata(ref, ref_rank);

    double set_time = -omp_get_wtime();
    monte_carlo.set_matrix(A);
    set_time += omp_get_wtime();

    for (int trial = 0; trial < NUM_TRIALS_SPEED; ++trial)
    {
        monte_carlo.set_param({.num_samples = samples,
                                      .max_num_terms = MAX_TERMS,
                                      .weight_cutoff = WEIGHT_CUTOFF,
                                      .seed = seed_gen(),
                                      .stream = seed_gen()});

        double t0 = omp_get_wtime();
        result = monte_carlo.funm(mocca::expm, gamma);
        res_diag = diag(result);
        double t1 = omp_get_wtime();

        print_results(res_diag, ref, ref_rank, graph, "subgraph", "RandFunm", A.rows(), A.size(), 1.0, gamma, samples,
                      t1 - t0 + set_time, 1, WEIGHT_CUTOFF);

    }
}

int main(int argc, char* argv[])
{
    if (argc != 6)
    {
        fmt::print(stderr, "Invalid Parameters. Usage: ./{} <type> <algorithm> <filename> <num_samples> <gamma>\n",
                   argv[0]);
        exit(EXIT_FAILURE);
    }

    std::string type = argv[1];
    std::string algo = argv[2];
    std::string filename = argv[3];
    index_t samples = atol(argv[4]);
    double gamma = atof(argv[5]);

    auto [A, ref] = import_data(type, filename, gamma);
    gamma = gamma == 0 ? 1 / max(norm_l1(A, mocca::rowwise)) : gamma;

    if (type == "subgraph")
    {
        if (algo == "MC") subgraph_MC(filename, A, ref, gamma, samples);
        else if (algo == "RandFunm") subgraph_full(filename, A, ref, gamma, samples);
        else subgraph_diag(filename, A, ref, gamma, samples);

    } else if (type == "total-comm")
    {
        total_comm_rand(filename, A, ref, gamma, samples);
    }

    return EXIT_SUCCESS;
}
