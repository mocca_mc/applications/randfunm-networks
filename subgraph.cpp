/*************************************************************************
    Copyright (C) 2022 Nicolas L. Guidotti. All rights reserved.

    This file is part of the MOCCA library, which is licensed under
    the terms contained in the LICENSE file.
 **************************************************************************/

#define MOCCA_NO_DEBUG

#include "common.h"
#include "mocca/monte_carlo/rand_funm.h"


int main(int argc, char* argv[])
{
    if (argc != 5)
    {
        fmt::print(stderr, "Invalid Parameters. Usage: ./{} <filename> <num_samples> <tol> <gamma>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    std::string filename = argv[1];
    index_t samples = atol(argv[2]);
    double tol = atof(argv[3]);
    double gamma = atof(argv[4]);

    mocca::RandFunm<CSRMatrix, mocca::kUniformProbability> monte_carlo;
    mocca::random::SplitMix64 seed_gen;

    Vector res;
    auto [A, ref] = import_data("subgraph", filename, gamma);
    gamma = gamma == 0 ? 1.0 / max(norm_l1(A, mocca::rowwise)) : gamma;

    Vector ref_rank(ref.size());
    rankdata(ref, ref_rank);

    double set_time = -omp_get_wtime();
    monte_carlo.set_matrix(A);
    set_time += omp_get_wtime();

    for (int trial = 0; trial < NUM_TRIALS_ACC; ++trial)
    {
        monte_carlo.set_param({.num_samples = samples,
                               .max_num_terms = MAX_TERMS,
                               .weight_cutoff = tol,
                               .seed = seed_gen(),
                               .stream = seed_gen()});

        double t0 = omp_get_wtime();
        res = monte_carlo.funm_diag(mocca::expm, gamma);
        double t1 = omp_get_wtime();

        print_results(res, ref, ref_rank, filename, "subgraph", "RandFunmDiag", A.rows(), A.size(), 1.0, gamma, samples,
                      t1 - t0 + set_time, omp_get_max_threads(), tol);
    }

    return EXIT_SUCCESS;
}
