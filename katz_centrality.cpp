/*************************************************************************
	Copyright (C) 2023 Nicolas L. Guidotti. All rights reserved.

	This file is part of the MOCCA library, which is licensed under 
	the terms contained in the LICENSE file.
 **************************************************************************/

//#define MOCCA_PARDISO_VERBOSE 1

#include "common.h"
#include "mocca/monte_carlo/classical_mc.h"
#include "mocca/monte_carlo/rand_funm.h"

#define CG_TOL 1E-8
#define CG_MAX_ITER 200
#define MC_TOL 1E-8

template<typename T>
double conjugate_gradient(const mocca::CSRMatrix<T>& A, mocca::Vector<T>& x, const mocca::Vector<T>& b, double tol, int max_iter)
{
    int iter = 1;
    double r_norm = 1;
    mocca::Vector<T> residual = b;
    mocca::Vector<T> p = b;
    mocca::Vector<T> Ap(residual.size(), 0);

    while (iter < max_iter and r_norm > tol)
    {
        mocca::mult(A, p, Ap);
        double r_dot = dot(residual, residual);
        double alpha = r_dot / dot(Ap, p);
        x = x + alpha * p;
        residual = residual - alpha * Ap;
        r_norm = norm_l2(residual);

        if (r_norm > tol)
        {
            double beta = dot(residual, residual) / r_dot;
            p = residual + beta * p;
            ++iter;
        }
    }

    return r_norm;
}

void test_cg(std::string& filename, const CSRMatrix& A, const Vector& v, Vector& u, double alpha)
{
    CSRMatrix B(A.rows(), A.cols(), A.size() + A.rows());
    CSRMatrix I;

    I.set_diagonal(A.rows(), 1);
    B = I - alpha * A;

    double t0 = omp_get_wtime();
    double residual_norm = conjugate_gradient(B, u, v, CG_TOL, CG_MAX_ITER);

    fmt::print("katz-centrality,");
    fmt::print("CG,");
    fmt::print("{},", omp_get_max_threads());
    fmt::print("{},", filename);
    fmt::print("{},", B.rows());
    fmt::print("{},", B.size());
    fmt::print("1,");
    fmt::print("{:.3g},", alpha);
    fmt::print("-1,");
    fmt::print("-1,");
    fmt::print("{:.3g},", residual_norm);
    fmt::print("{:.3g},", residual_norm);
    fmt::print("{:.3g},", residual_norm);
    fmt::print("1,");
    fmt::print("1,");
    fmt::print("{:.3g}\n", omp_get_wtime() - t0);
}

void test_classical_mc(std::string& filename, index_t samples, CSRMatrix& A, Vector& v, const Vector& ref,
                       double alpha)
{
    mocca::random::SplitMix64 seed_gen(PCG32_DEFAULT_SEED, PCG32_DEFAULT_STREAM);
    mocca::ClassicalMC<CSRMatrix> solver(A);
    Vector u(A.rows(), 0);

    Vector ref_rank(ref.size());
    rankdata(ref, ref_rank);

    for (int i = 0; i < NUM_TRIALS_SPEED; ++i)
    {
        uint64_t seed = seed_gen();
        uint64_t stream = seed_gen();
        solver.set_param({.num_samples = samples, .weight_cutoff = MC_TOL, .seed = seed, .stream = stream});

        double t0 = omp_get_wtime();
        u = solver.funm(mocca::inverse, v, alpha);
        fmt::print(stderr, "{}\n", u);

        print_results(u, ref, ref_rank, filename, "katz-centrality", "ClassicalMC", A.rows(), A.size(), 1, alpha, samples,
                      omp_get_wtime() - t0, omp_get_max_threads(), MC_TOL);
    }
}

void test_rand_action(std::string& filename, index_t samples, CSRMatrix& A, Vector& v, const Vector& ref, double alpha)
{
    mocca::random::SplitMix64 seed_gen(PCG32_DEFAULT_SEED, PCG32_DEFAULT_STREAM);
    mocca::RandFunm<CSRMatrix> solver(A);
    Vector u(A.rows(), 0);

    Vector ref_rank(ref.size());
    rankdata(ref, ref_rank);

    for (int i = 0; i < NUM_TRIALS_SPEED; ++i)
    {
        uint64_t seed = seed_gen();
        uint64_t stream = seed_gen();
        solver.set_param({.num_samples = samples, .weight_cutoff = MC_TOL, .seed = seed, .stream = stream});

        double t0 = omp_get_wtime();
        u = solver.funm(mocca::inverse, v, alpha);
        print_results(u, ref, ref_rank, filename, "katz-centrality", "RandFunmAction", A.rows(), A.size(), 1, alpha, samples,
                      omp_get_wtime() - t0, omp_get_max_threads(), MC_TOL);
    }
}

int main(int argc, char* argv[])
{
    if (argc != 3)
    {
        std::fprintf(stderr, "Invalid Parameters. Usage: %s <filename> <num_samples>", argv[0]);
        exit(EXIT_FAILURE);
    }

    std::string filename = argv[1];
    index_t samples = atol(argv[2]);

    CSRMatrix A;
    mocca::io::read_hdf5("input/" + filename + ".h5", "A", A);
    Vector v(A.rows(), 1);
    Vector u(A.rows(), 0);

    double alpha =  0.85 / max(norm_l1(A, mocca::rowwise));

    test_cg(filename, A, v, u, alpha);
    test_rand_action(filename, samples, A, v, u, alpha);
    test_classical_mc(filename, samples, A, v, u, alpha);

    return EXIT_SUCCESS;
}


