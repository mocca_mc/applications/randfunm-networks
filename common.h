/*************************************************************************
    Copyright (C) 2023 Nicolas L. Guidotti. All rights reserved.

    This file is part of the RandNetwork library, which is licensed under
    the terms contained in the LICENSE file.
 **************************************************************************/

#include <cmath>
#include <fmt/format.h>
#include <omp.h>

#include "mocca/io/hdf5.h"
#include "mocca/random/splitmix.h"
#include "mocca/matexpr/norm.h"
#include "mocca/matexpr/ewise_nullary_op.h"
#include "mocca/datatypes/csr_matrix.h"
#include "mocca/datatypes/vector.h"
#include "mocca/datatypes/matrix.h"

#define NUM_TRIALS_ACC 10
#define NUM_TRIALS_SPEED 3
#define MAX_TERMS 256

using index_t = mocca::index_t;
using Vector = mocca::Vector<double>;
using CSRMatrix = mocca::CSRMatrix<double>;
using Matrix = mocca::Matrix<double>;

void normalize(Vector& vec)
{
    double max_v = max(vec);
    double min_v = min(vec);
    vec = (vec - min_v) / (max_v - min_v);
}

double correlation(Vector& x, Vector& y)
{
    double mean_x = sum(x) / x.size();
    double mean_y = sum(y) / y.size();
    return sum((x - mean_x) * (y - mean_y)) / norm_l2(x - mean_x) / norm_l2(y - mean_y);
}

void rankdata(const Vector& x, Vector& rank)
{
    rank = mocca::arange(0.0, double(x.size()));
    std::sort(rank.begin(), rank.end(), [&](index_t i, index_t j) {
        return x[i] > x[j];
    });
}

void print_results(Vector& res, const Vector& ref, const Vector& ref_rank, std::string filename, std::string centrality, std::string algorithm,
                   index_t rows, index_t nz, double alpha, double gamma, index_t samples, double time, int nt,
                   double tol)
{
    double abs_error = mocca::norm_max(res - ref);
    double rel_error = abs_error / mocca::norm_max(ref);
    double l2_error = mocca::norm_l2(res - ref);

    Vector rank_100;
    rankdata(res, rank_100);

    Vector rank_10 = mocca::init_vector<double>(0.1 * ref.size(), [&](auto i){
        return rank_100[i];
    });

    Vector rank_1 = mocca::init_vector<double>(0.01 * ref.size(), [&](auto i){
        return rank_100[i];
    });

    Vector ref_rank_10 = mocca::init_vector<double>(0.1 * ref.size(), [&](auto i){
        return ref_rank[i];
    });

    Vector ref_rank_1 = mocca::init_vector<double>(0.01 * ref.size(), [&](auto i){
        return ref_rank[i];
    });

    double corr_10 = correlation(rank_10, ref_rank_10);
    double corr_1 = correlation(rank_1, ref_rank_1);

    fmt::print("{},", centrality);
    fmt::print("{},", algorithm);
    fmt::print("{},", nt);
    fmt::print("{},", filename);
    fmt::print("{},", rows);
    fmt::print("{},", nz);
    fmt::print("{:.3g},", alpha);
    fmt::print("{:.3g},", gamma);
    fmt::print("{},", samples);
    fmt::print("{:.3g},", tol);
    fmt::print("{:.6g},", abs_error);
    fmt::print("{:.6g},", rel_error);
    fmt::print("{:.6g},", l2_error);
    fmt::print("{:.6g},", corr_10);
    fmt::print("{:.6g},", corr_1);
    fmt::print("{:.3g}\n", time);
}

std::tuple<CSRMatrix, Vector> import_data(std::string type, std::string graph, double gamma)
{
    CSRMatrix A;
    Vector ref;
    std::string refname = fmt::format("reference/{}-{}-{}.h5", graph, gamma, type);
    mocca::io::read_hdf5("input/" + graph + ".h5", "A", A);
    mocca::io::read_hdf5(refname, "result", ref);
    return {std::move(A), std::move(ref)};
}